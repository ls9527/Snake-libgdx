package xyz.zzzxb.snake.ui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;

import java.awt.*;


/**
 * @author Zzzxb  2019/10/12 20:38
 * @description:
 */
public class Wall {
    private int blockSize = 16;
    private Color color;
    private Pixmap pixmap;
    private Texture texture;

    public Wall() {
        pixmap = new Pixmap(blockSize, blockSize, Pixmap.Format.RGBA8888);
        color = Color.SKY;
        pixmap.setColor(color);
        pixmap.fillRectangle(0, 0, blockSize, blockSize);
        texture = new Texture(pixmap);
        init();
    }

    public void init() {
    }

    public void draw(Batch batch) {
        for (int i = 0; i < Gdx.graphics.getWidth(); i+=16) {
            batch.draw(texture,i,0);
            batch.draw(texture,i,Gdx.graphics.getHeight()-16);
            if (i != 0 && i != Gdx.graphics.getHeight()-16) {
                batch.draw(texture,0, i);
                batch.draw(texture,Gdx.graphics.getWidth()-16, i);
            }
        }
    }


    public void dispose() {
        texture.dispose();
        pixmap.dispose();
    }
}
