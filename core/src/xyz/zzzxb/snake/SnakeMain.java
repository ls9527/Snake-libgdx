package xyz.zzzxb.snake;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import xyz.zzzxb.snake.game.GameController;
import xyz.zzzxb.snake.game.GameSound;
import xyz.zzzxb.snake.game.GameWorld;

public class SnakeMain extends ApplicationAdapter {
    private GameWorld gameWorld;
    private GameController gameController;
    private Texture texture;
    private Sprite sprite;
    private SpriteBatch batch;
    public static boolean runGame;

    @Override
    public void create() {
        gameController = new GameController();
        gameWorld = new GameWorld(gameController);
        texture = new Texture(Gdx.files.internal("title.png"));
        sprite = new Sprite(texture);
        batch = new SpriteBatch();
        runGame = true;
    }

    @Override
    public void render() {
        Gdx.gl.glClearColor(0.860f, 1, 1, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        welcome(Gdx.graphics.getDeltaTime());
    }

    private boolean onOff = true;
    private boolean start = false;
    private float count = 0f;

    public void welcome(float deltaTime) {
        if (!start) {
            batch.begin();
            sprite.draw(batch, count);
            if (onOff) {
                count = count + deltaTime/2 < 1f ? count + deltaTime / 2 : 1f;
                if (Gdx.input.isKeyPressed(Input.Keys.ENTER))
                    onOff = false;
            } else {
                count = count - deltaTime/2 > 0f ? count - deltaTime / 2 : 0f;
                if (count <= 0f) {
                    start = true;
                    gameWorld.init();
                    batch.dispose();
                }
            }
            batch.end();
        }else {
            gameWorld.render();
            gameWorld.update(Gdx.graphics.getDeltaTime());
        }
    }

    public void pause () {
        GameSound.dispose();
        runGame = false;
    }

    @Override
    public void resume () {
        new GameSound();
        runGame = true;
    }

    @Override
    public void dispose() {
        gameWorld.dispose();
    }
}
