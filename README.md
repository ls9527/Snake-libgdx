# README

* **About**

    * `Project : ` **Snake**
    * `Version : ` **1.0**
    * `author: ` **Zzzxb**
    * `Data : ` **2019-10-13**
    
* **Bug**
    * 方向键按的太快,导致图像刷新跟不上数据的变化
    * 开始游戏后切换窗口会被强制退出
    * R键刷新数据过多也会被强制退出
    * 食物会刷新在蛇身上

## Demo

![Demo](demo.gif)